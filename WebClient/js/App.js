var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(['$routeProvider', function ($routeProvider) {

    $routeProvider
        .when('/Home', {
            templateUrl: 'views/Home.html',
            controller: 'myCtrl'
        })
        .when('/Shop', {
            templateUrl: 'views/Shop.html',
            controller: 'shopCtrl'
        })
        .when('/Shop/:id', {
            templateUrl: 'views/itemPage.html',
            controller: 'itemCtrl'
        })
        .when('/About',{
            templateUrl: 'views/About.html',
            controller: 'myCtrl'
        })
        .when('/Contact', {
            templateUrl: 'views/Contact.html',
            controller: 'myCtrl'
        })
        .when('/Contact-success', {
            templateUrl: 'views/Contact.success.html',
            controller: 'myCtrl'
        })
        .otherwise({
            redirectTo: '/Home'
        })
}]);

myApp.controller('myCtrl', ['$scope','$http', function($scope, $http) {
    // var container = document.getElementById("insta");
    // if(container){
    //     container.innerHTML = '<a href="https://instawidget.net/v/user/adikastyle" id="link-8d7fa1b9965f6a2c7d5a12529e40c0ffc88cb7163caf98f838549c556791d760">@adikastyle</a><script src="https://instawidget.net/js/instawidget.js?u=8d7fa1b9965f6a2c7d5a12529e40c0ffc88cb7163caf98f838549c556791d760&width=800px"></script>';
    // }
}]);

myApp.controller('shopCtrl', ['$scope','shopService', '$routeParams',
    function($scope, shopService, $routeParams) {
    shopService.getAllItems().then(function(data){
        $scope.items = data;
    })

}]);

myApp.controller('itemCtrl', ['$scope','shopService', '$routeParams',
    function($scope,shopService, $routeParams) {
        $scope.currentID = $routeParams.id;
        if($scope.currentID != undefined){
            shopService.getItem($scope.currentID).then(function(data){
                $scope.currentItem = data;
            });
        }
}]);
