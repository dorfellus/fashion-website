var myApp = angular.module('myApp');
myApp.directive('itemPreview',[function(){
    return{
        restrict: 'E',
        scope:{
            id: '=',
            name: '=',
            price: '=',
            img: '='
        },
        templateUrl: 'views/itemPreview.directive.html',
        controller:function($scope, $location){
            $scope.OnItemClick = function(){
                $location.url('/Shop/' + $scope.id);
            }
        }
    }
}]);
