var myApp = angular.module('myApp');
myApp.directive('itemPage',[function(){
    return{
        restrict: 'E',
        scope:{
            name: '=',
            price: '=',
            img: '='
        },
        templateUrl: 'views/itemPage.directive.html',
        controller:function($scope){

        }
    }
}]);
