var myApp = angular.module('myApp');
myApp.factory('shopService', ['$http', '$q', function ($http, $q) {

    var items = [];
    function getAllItems () {
        var deferred = $q.defer();
        if (items.length == 0)
        {
            $http.get('data/items.json')
                .then(function (response) {
                    items = response.data;
                    deferred.resolve(items);
                })
        }
        else
        {
            deferred.resolve(items);
        }
        return deferred.promise;

    };

    function getItem(id) {
        var deferred = $q.defer();
        getAllItems().then(function(){
            for (var item in items){
                if (items[item].ID == id){
                    deferred.resolve(items[item]);
                }
            }
        });
        return deferred.promise;
    }

    return{
        getAllItems:getAllItems,
        getItem: getItem
    }
}]);
