Fashion E-commerce Platform
===========================
This project is a fashion e-commerce platform and was my final project on SheCodes course.
The website uses a server wrote using C# technology in order to make queries from a MySQL Database contains all the data about the items for sale.

Demo:
https://dorothys-closet.firebaseapp.com/

Tech Stack:
1. MySQL Database
2. ASP.NET Web API
3. AngularJS
4. Bootstrap 4
5. C#