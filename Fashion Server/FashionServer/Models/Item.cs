﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FashionServer.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public decimal Price { get; set; }
    }
}