﻿using FashionServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;

namespace FashionServer.Controllers
{
    public class ItemsController : ApiController
    {
        string connectionString;

        public ItemsController()
        {
            connectionString = "Server=localhost;Port=3306;Database=fashiondb;Uid=root;password=Dd123456";
        }

        public IHttpActionResult GetAllItems()
        {
            string commandText = "select * from fashiondb.products";
            List<Item> items = CreateCommand(commandText);
            return Ok(items);
        }

        public IHttpActionResult GetItem(int id)
        {
            string commandText = "select * from fashiondb.products where ID = " + id;
            List<Item> items = CreateCommand(commandText);
            if (items.Count == 0)
            {
                return NotFound();
            }
            return Ok(items[0]);
        }

        public List<Item> CreateCommand(string commandText)
        {
            List<Item> items = new List<Item>();
            MySqlConnection conn = new MySqlConnection(connectionString);
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = commandText;
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                Item currItem = new Item();
                currItem.Id = int.Parse(reader["ID"].ToString());
                currItem.Name = reader["Name"].ToString();
                currItem.Price = Decimal.Parse(reader["Price"].ToString());
                currItem.Type = int.Parse(reader["Type"].ToString());
                items.Add(currItem);
            }
            conn.Close();
            return items;
        }
    }
}
